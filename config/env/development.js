export default {
  env: 'development',
  db: 'mongodb://localhost:27017/aleko_art',
  port: 9090,
  jwtSecret: 'aleko-art-api-secret',
  jwtDuration: '2 hours'
};
