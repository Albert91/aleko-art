import express from 'express';
import userCtrl from '../../controllers/users';
import auth from '../../../config/jwt';

const router = express.Router();

// TODO refactor users routing
router.route('/')
  .get(userCtrl.list)

  /** POST /users - Create new user */
 // .post(userCtrl.create);

router.route('/:userId')
  /** GET /users/:userId - Get user */
  .get(auth, userCtrl.get)

  /** PUT /api/users/:userId - Update user */
  .put(auth, userCtrl.update)


/** Load user when userId route parameter is hit */
router.param('userId', userCtrl.load);

export default router;
