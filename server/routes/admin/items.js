import express from 'express';
import itemCtrl from '../../controllers/items';
import auth from '../../../config/jwt';

const router = express.Router();

// TODO refactor users routing
router.route('/')
  .get(itemCtrl.list)

  /** POST /items - Create new item */
  .post(itemCtrl.create);

router.route('/:userId')
  /** GET /items/:itemId - Get item */
  .get(itemCtrl.get)

  /** PUT /items/:itemId - Update item */
  .put(auth, itemCtrl.update)


/** Load user when userId route parameter is hit */
router.param('itemId', itemCtrl.load);

export default router;
