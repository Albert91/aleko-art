import express from 'express';
import validate from 'express-validation';
import categoriesCtrl from '../../controllers/categories';
import validations from '../validation/categories';

const router = express.Router();

router.route('/')
  /** GET /categories - Get list of categories */
  .get(categoriesCtrl.list)

  /** POST /categories - Create new category */
  .post(validate(validations.createCategory),
      categoriesCtrl.create);

router.route('/:taskId')
  /** GET /categories/:taskId - Get category */
  .get(categoriesCtrl.get)

  /** PUT /categories/:taskId - Update category */
  .put(validate(validations.updateCategory),
      categoriesCtrl.update)

  /** DELETE /categories/:categoryId - Delete category */
  .delete(categoriesCtrl.remove);

/** Load task when API with taskId route parameter is hit */
router.param('categoryId', validate(validations.getCategory));
router.param('categoryId', categoriesCtrl.load);

export default router;
