'use strict';
Object.defineProperty(exports, "__esModule", {
    value: true
});

var express = require('express');
express = _interopRequireDefault(express);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var router = express.default.Router();

/* GET indexPage page. */
router.get('/', function (req, res, next) {
  return res.render('./admin/categories', { title: 'Login' });
});

exports.default = router;
module.exports = exports['default'];