import express from 'express';
const router = express.Router();

/* GET indexPage page. */
router.get('/', function (req, res, next){
   res.render('./client/pages/index', {title: 'Login'});
});

export default router;