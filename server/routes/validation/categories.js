import Joi from 'joi';

export default {
  // POST /categories
  createCategory: {
    body: {
      name: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
    }
  },

  // GET /categories/:taskId
  getCategory: {
    params: {
      categoryId: Joi.string().regex(/^[0-9a-fA-F]{24}$/).required()
    }
  },

  // PUT /categories/:taskId
  updateCategory: {
    body: {
      name: Joi.string().regex(/^[0-9a-fA-F]{24}$/)
    }
  }
};
