import express from 'express';
import authRoutes from './auth';
import indexAdminRoute from './admin/indexPage';
import loginRoute from './login';
import categoriesRoutes from './admin/categories';
import itemsRoutes from './admin/items';

const router = express.Router();

//router.get('/', indexPage);

/** GET / - Render admin pages **/
router.use('/admin', indexAdminRoute);

router.use('/auth', authRoutes);
router.use('/login', loginRoute);
router.use('/admin/categories', categoriesRoutes);
router.use('/admin/items', itemsRoutes);

export default router;
