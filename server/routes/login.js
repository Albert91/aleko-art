'use strict';
import express from 'express';
const router = express.Router();

/* GET login page.  */

router.route('/')
    // show the form (GET http://localhost:8080/login)
    .get(function(req, res) {
       res.render('./admin/login', {title: 'Login'});
    })

    // process the form (POST http://localhost:8080/login)
    .post(function(req, res) {
    })

export default router;