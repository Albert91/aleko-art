import request from 'supertest-as-promised';
import httpStatus from 'http-status';
import { expect } from 'chai';
import sinon from 'sinon';
import app from '../../../index';
import { clearDatabase } from '../../helpers/ClearDB';
import User from '../../models/user';
import Task from '../../models/category';

require('sinon-mongoose');
require('sinon-as-promised');

describe('## Categories API Tests', () => {

  let sandbox, user;

  before((done) => {
    User.create({
      username: 'testuser',
      password: 'testuser'
    }).then((u) => {
      user = u;
      done();
    })
  });

  beforeEach((done) => {
    clearDatabase(() => {
      sandbox = sinon.sandbox.create();
      done();
    });
  });

  afterEach((done) => {
    sandbox.restore();
    done();
  });

  describe('### GET /categories', () => {

  });

  describe('### GET /categories/:categoryId', () => {

  });

  describe('### POST /categories', () => {
    it('should return the created category successfully', (done) => {
      request(app)
        .post('/categories')
        .send({
          name: 'test category'
        })
        .expect(httpStatus.OK)
        .then(res => {
          expect(res.body.name).to.equal('this is a test task');
          done();
        });
    });

    it('should return Internal Server Error when mongoose fails to save category', (done) => {
      const createStub = sandbox.stub(Task, 'create');
      createStub.rejects({});
      request(app)
        .post('/categories')
        .send({
          name: 'this is a test category'
        })
        .expect(httpStatus.INTERNAL_SERVER_ERROR)
        .then(() => done());
    });

    it('should return Bad Request when missing name', (done) => {
      request(app)
        .post('/categories')
        .send({
          name: 'this is a test category'
        })
        .expect(httpStatus.BAD_REQUEST)
        .then(() => done());
    });
  });

  describe('### PUT /categories/:categoryId', () => {

  });

  describe('### DELETE /categories/:categoryId', () => {

  });

});