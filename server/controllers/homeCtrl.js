/**
 * Created by Albert on 8/7/2017.
 */
import Categories from '../models/categories';
import Users from '../models/users';

function load(req, res, next, id) {
    Categories.find()
        .exec()
        .then((category) => {
            if (!category) {
                return res.status(404).json({
                    status: 400,
                    message: 'Category not found'
                });
            }
            req.dbCategory = category;
            return next();
        }, (e) => next(e));
}

function get(req, res) {
    return res.json(req.dbCategory);
}

function list(req, res, next) {
    const { limit = 50, skip = 0 } = req.query;
    Categories.find()
        .skip(skip)
        .limit(limit)
        .exec()
        .then((categories) => res.json(categories),
            (e) => next(e));
}

export default { load, get, list };