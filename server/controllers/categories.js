import Category from '../models/category';

function load(req, res, next, id) {
    Category.findById(id)
    .exec()
    .then((category) => {
      req.dbTask = category;
      return next();
    }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbTask);
}

function create(req, res, next) {
    Category.create({
      user: req.body.user,
      description: req.body.description
    })
    .then((savedTask) => {
      return res.json(savedTask);
    }, (e) => next(e));
}

function update(req, res, next) {
  const category = req.dbTask;
  Object.assign(category, req.body);

  category.save()
    .then(() => res.sendStatus(204),
      (e) => next(e));
}

function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Category.find()
    .skip(skip)
    .limit(limit)
    .exec()
    .then((tasks) => res.json(tasks),
      (e) => next(e));
}

function remove(req, res, next) {
  const category = req.dbTask;
  category.remove()
    .then(() => res.sendStatus(204),
      (e) => next(e));
}

export default { load, get, create, update, list, remove };
