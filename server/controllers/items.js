import Item from '../models/item';

function load(req, res, next, id) {
  Item.findById(id)
    .exec()
    .then((item) => {
      if (!item) {
        return res.status(404).json({
          status: 400,
          message: 'Item not found'
        });
      }
      req.dbItem = item;
      return next();
    }, (e) => next(e));
}

function get(req, res) {
  return res.json(req.dbItem);
}

function create(req, res, next) {
  Item.create({
      name: req.body.name,
      description: req.body.description,
      img: req.body.img,
      cat_id: req.body.cat_id
    })
    .then((savedItem) => {
      return res.json(savedItem);
    }, (e) => next(e));
}

function update(req, res, next) {
  const item = req.dbItem;
  Object.assign(item, req.body);

  item.save()
    .then(() => res.sendStatus(204),
      (e) => next(e));
}

function list(req, res, next) {
  const { limit = 50, skip = 0 } = req.query;
  Item.find()
    .skip(skip)
    .limit(limit)
    .exec()
    .then((items) => res.json(items),
      (e) => next(e));
}

export default { load, get, create, update, list };
