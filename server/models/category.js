import mongoose from 'mongoose';

const CategoriesSchema = new mongoose.Schema({
  id: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
  },
  name: {
    type: String,
    required: true,
    trim: true
  }
});

export default mongoose.model('Category', CategoriesSchema);

