import mongoose from 'mongoose';

const ItemSchema = new mongoose.Schema({
  name: {
    type: String,
    required: true,
    trim: true
  },
  description: {
    type: String,
    required: false,
    trim: true
  },
  img: {
    type: String,
    required: true,
    trim: true
  },
  cat_id: {
    type: Number,
    required: true
  }
});

export default mongoose.model('Item', ItemSchema);
